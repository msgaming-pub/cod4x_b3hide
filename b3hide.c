#include "../pinc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#define PLUGIN_NAME "Morning Star Gaming Chat Control Plugin by Brunz"
#define PLUGIN_DESC "This plugin is used to hide b3 or other."
#define PLUGIN_DESC_LONG "Allows setting of prefix variable to match against first character of message sent and hides the respective chat."
#define PLUGIN_VER_MAJ 1
#define PLUGIN_VER_MIN 0

typedef struct {
    int level;
} clientData_t;
clientData_t *clients;

void checkinClient();
qboolean getIntArg(int *var, int arg_i);

xcommand_t client_checkin = checkinClient;

cvar_t *cvar_b3Prefix;
cvar_t *cvar_b3Hide;
cvar_t *cvar_b3HideLvl;

void initSlot(int slot) {
    clientData_t client;
    client.level = 0;
    clients[slot] = client;
}

void checkinClient() {
    int slot;
    int level;
    clientData_t client;

    if(getIntArg(&slot, 1) == qtrue && getIntArg(&level, 2) == qtrue && (slot >= 0 && slot <= Plugin_GetSlotCount())) {
        client.level = level;
        clients[slot] = client;

        if(level >= Plugin_Cvar_GetInteger(cvar_b3HideLvl)) {
            Plugin_BoldPrintf(slot, "^5You are checked in with ^3Level ^2%i.\n^5Your ^3!b3 ^5commands are now ^3hidden!\n", level);
            Plugin_BoldPrintf(slot, "^5Use ^3!auth <password> ^5to complete the ^3authentication.^7\n");
        }
    } else {
        Plugin_Printf("Invalid Arguments, usage:\n");
        Plugin_Printf("checkinlvl <slot> <level>\n");
    }
}

qboolean getIntArg(int *var, int arg_i) {
    char *argstr;
    int j = 0;
    argstr = Plugin_Cmd_Argv(arg_i);
    if(strlen(argstr) == 0) {
        return qfalse;
    }

    for (j = 0; argstr[j]; j++) {
		if (argstr[j] < '0' || argstr[j] > '9') {
			return qfalse;
		}
	}

    *var = atoi(argstr);
    return qtrue;
}

PCL void OnMessageSent(char *message, int slot, qboolean *show, int mode) {
    char b3prefix = Plugin_Cvar_GetString(cvar_b3Prefix)[0];
    int b3hidelvl = Plugin_Cvar_GetInteger(cvar_b3HideLvl);
    qboolean b3hide = Plugin_Cvar_GetBoolean(cvar_b3Hide);

    int offset = 0;
    if(message[0] == 0x15) {
        offset = 1;
    }

    if(b3hide == qtrue && message[offset] == b3prefix && clients[slot].level >= b3hidelvl) {
            *show = qfalse;
            //Plugin_ChatPrintf(slot, "* %s", message);
    }
}

PCL void OnPlayerConnect(int clientnum, netadr_t* netaddress, char* pbguid, char* userinfo, int authstatus, char* deniedmsg,  int deniedmsgbufmaxlen) {
    initSlot(clientnum);
    return;
}

PCL void OnPlayerDC(client_t* client, const char* reason){
	initSlot(NUMFORCLIENT(client));
    return;
}

PCL int OnInit() {
    int maxclients = Plugin_GetSlotCount();

    if(clients != NULL){
        Plugin_Free(clients);
    }
    clients = (clientData_t *)Plugin_Malloc(sizeof(clientData_t)*maxclients);
    memset(clients,0x00,sizeof(clientData_t)*maxclients);

    int i;
    for(i = 0; i < maxclients; i++) {
        initSlot(i);
    }

    cvar_b3Prefix = Plugin_Cvar_RegisterString("b3Prefix", "!", 0, "Prefix for B3 commands (usually !) to hide");
    cvar_b3Hide = Plugin_Cvar_RegisterBool("b3Hide", qtrue, 0, "Hide B3 commands (1 = On, 0 = Off)");
    cvar_b3HideLvl = Plugin_Cvar_RegisterInt("b3HideLvl", 20, 0, 100, 0, "Hides B3 commands if client's level >= b3HideLvl (default: 20)");

    Plugin_AddCommand("client_checkin", client_checkin, 95);

    return 0;
}

PCL void OnInfoRequest(pluginInfo_t *info) {
    // Memory pointed by info is allocated by the server binary, just fill in the fields
    // =====  MANDATORY FIELDS  =====
    info->handlerVersion.major = PLUGIN_HANDLER_VERSION_MAJOR;
    info->handlerVersion.minor = PLUGIN_HANDLER_VERSION_MINOR;	// Requested handler version

    // =====  OPTIONAL  FIELDS  =====
    info->pluginVersion.major = PLUGIN_VER_MAJ;
    info->pluginVersion.minor = PLUGIN_VER_MIN;	// Plugin version
    strncpy(info->fullName, PLUGIN_NAME,sizeof(info->fullName)); //Full plugin name
    strncpy(info->shortDescription, PLUGIN_DESC,sizeof(info->shortDescription)); // Short plugin description
    strncpy(info->longDescription, PLUGIN_DESC_LONG,sizeof(info->longDescription));
}
